#include <stdio.h>
#define __USE_MISC 1
#include "mem_internals.h"
#include "mem.h"
#include "print_info.h"
#include "util.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

#define TEST(number, ...)\
  bool test_##number(){\
    printf("Begin test_");\
    printf("%u", number);\
    void* memory = heap_init(5000);\
    debug_heap( stderr, memory);\
    if ( !memory ) return  print_info("Не удалось выделить память под кучу.", stderr),false;\
    __VA_ARGS__\
    munmap( memory, size_from_capacity(blk_header_1->capacity).bytes );\
    return true;\
}

#define ALLOC_BLOCK(number, size)\
  void* block_##number = _malloc(size);\
  struct block_header* blk_header_##number = (struct block_header*)((block_##number) - offsetof( struct block_header, contents ));

#define FREE_BLOCK(number)\
  _free( block_##number );

#define BLOCK_SIZE_1 500
#define BLOCK_SIZE_2 1500
#define BLOCK_SIZE_3 2500
#define BLOCK_SIZE_4 500

// Обычное успешное выделение памяти.
TEST(1,
ALLOC_BLOCK(1, BLOCK_SIZE_1)
debug_heap( stderr, memory );
if ( !block_1 ) return print_info("Не удалось выделить память под 1-ый блок.", stderr),false;
FREE_BLOCK(1)
)

// Освобождение одного блока из нескольких выделенных.

TEST(2,
ALLOC_BLOCK(1, BLOCK_SIZE_1)
ALLOC_BLOCK(2, BLOCK_SIZE_2)
ALLOC_BLOCK(3, BLOCK_SIZE_3)
debug_heap( stderr, memory );
if ( !block_1 || !block_2 || !block_3 ) return print_info("Не удалось выделить память под блоки.", stderr), false;
FREE_BLOCK(2)
debug_heap( stderr, memory );
if ( !blk_header_2->is_free ) return print_info("Не очистился второй блок.", stderr),false;
FREE_BLOCK(3)
FREE_BLOCK(1)
)

// Освобождение двух блоков из нескольких выделенных.
TEST(3, 
ALLOC_BLOCK(1, BLOCK_SIZE_1)
ALLOC_BLOCK(2, BLOCK_SIZE_2)
ALLOC_BLOCK(3, BLOCK_SIZE_3)
ALLOC_BLOCK(4, BLOCK_SIZE_4)
debug_heap( stderr, memory );
if ( !block_1 || !block_2 || !block_3 || !block_4 ) return print_info("Не удалось выделить память под блоки.", stderr),false;
FREE_BLOCK(3)
debug_heap( stderr, memory );
if ( !blk_header_3->is_free ) return print_info("Не удалось освободить третий блок.", stderr),false;
FREE_BLOCK(2)
debug_heap( stderr, memory );
if ( !blk_header_2->is_free || blk_header_2->next != blk_header_4 || blk_header_2->capacity.bytes != BLOCK_SIZE_2 + size_from_capacity(blk_header_3->capacity).bytes ) return print_info("Не удалось освободить второй блок или не переписан указатель на верный блок.", stderr),false;
FREE_BLOCK(4)
FREE_BLOCK(1))

#undef BLOCK_SIZE_1
#define BLOCK_SIZE_1 1500
#define BLOCK_SIZE_2 1500
// Память закончилась, новый регион памяти расширяет старый.
TEST(4, 
ALLOC_BLOCK(1, BLOCK_SIZE_1)
ALLOC_BLOCK(2, BLOCK_SIZE_2)
debug_heap( stderr, memory );
ALLOC_BLOCK(3, BLOCK_SIZE_3)
debug_heap( stderr, memory );
if ( !block_1 || !block_2 || !block_3 ) return print_info("Не удалось выделить память под блоки.", stderr),false;
if ( ( struct block_header * )(blk_header_2->contents + BLOCK_SIZE_2) != blk_header_3 || blk_header_2->next != blk_header_3 ) return print_info("Неверно выделена память для 3 блока или неверный указатель на 3 блок", stderr),false;
FREE_BLOCK(2)
FREE_BLOCK(3)
FREE_BLOCK(1))

#undef BLOCK_SIZE_1
#define BLOCK_SIZE_1 3000
#undef BLOCK_SIZE_2
#define BLOCK_SIZE_2 2500

// Память закончилась, старый регион памяти не расширить из-за другого выделенного диапазона адресов, новый регион выделяется в другом месте.
TEST(5, 
ALLOC_BLOCK(1, BLOCK_SIZE_1)
debug_heap( stderr, memory );
if ( !block_1 ) return print_info("Не удалось выделить память под 1-ый блок", stderr),false;
void * new_memory = mmap((blk_header_1->contents + BLOCK_SIZE_1), REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0 );
if ( new_memory == MAP_FAILED ) return print_info("Память не удалось выделить, проверка невозможна.", stderr),false;
ALLOC_BLOCK(2, BLOCK_SIZE_2)
debug_heap( stderr, memory );
if ( !block_2 || blk_header_2 == ( struct block_header * )(blk_header_1->contents + BLOCK_SIZE_1) ) return print_info("Не удалось выделить память под 2-ый блок || память для второго блока выделилась не там.", stderr),false;
FREE_BLOCK(2)
FREE_BLOCK(1))

bool run_test(){
  if( test_1() && test_2() && test_3() && test_4() && test_5() ) return true;
  return print_info("Тесты упали :(", stderr),false; 
}

int main(){
  if(run_test()) return 0;
  return 1;
}
