#include "print_info.h"

void println_info( const char* const message, FILE* stream   ){
  print_info( message, stream );
  print_info( "\n", stream );
}

void print_info( const char* const message, FILE* stream   ){
  fprintf( stream, "%s" , message );
}

void print_pointer( void *  message, FILE* stream   ){
  fprintf( stream, "%p" , message );
}

void print_size_t( size_t message, FILE* stream   ){
  fprintf( stream, "%zu" , message );
}
