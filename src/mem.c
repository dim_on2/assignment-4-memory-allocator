#define _DEFAULT_SOURCE

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

static size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
  size_t size = size_from_capacity( (block_capacity) { .bytes = query } ).bytes;
  // respect to the addr ( Don't interpret addr as a hint ) - MAP_FIXED
  //  and never  clobbers a preexisting mapped range - MAP_FIXED_NOREPLACE;
  void* region_addr = map_pages( addr, region_actual_size( size ), MAP_FIXED_NOREPLACE );
  // если не удалось выделить после кучи, позволяем mmap выделить память где угодно.
  region_addr = region_addr == MAP_FAILED ? map_pages( addr, region_actual_size( size ), 0 ) : region_addr;
  // Если опять не удалось выделить - возвращаем невалидный регион
  if ( region_addr == MAP_FAILED ) return REGION_INVALID;
  bool is_extends = region_addr == addr;
  struct region region = { .addr = region_addr, .extends = is_extends, .size = region_actual_size( size ) };
  // инициализировать регион блоком
  block_init( region.addr, (block_size) { .bytes= region.size }, NULL );
  return region;
}

static void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;
  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
  if ( !block_splittable( block, query ) ) return false;
  block_capacity needed_capacity = ( block_capacity){ query };
  // struct block_header * new_block = block + size_from_capacity(needed_capacity).bytes;
  // Я не обратил внимание, что если оставить так как в строке выше - то прибавляется sizeof(struct block_header) * size_from_capacity(needed_capacity).bytes
  struct block_header * new_block = (struct block_header *)(block->contents + needed_capacity.bytes);
  block_size size_new_block = (block_size){block->capacity.bytes - needed_capacity.bytes};
  // create new block with beginning from block + size_from_capacity(( block_capacity){ .bytes = query })
  block_init( new_block, size_new_block, block->next );
  // fix given block's parameter
  block->capacity = needed_capacity;
  block->next = new_block;
  return true;
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  // checking merging possibility
  if ( block->next == NULL || !mergeable( block, block->next ) ) return false;
  // increase capasity - summury capacity by current block capasity and next block size   
  block->capacity.bytes += size_from_capacity(block->next->capacity).bytes;
  // current block pointer to next block -> next block pointer to next block
  block->next = block->next->next;
  return true;
}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz ) {

// предыдущая версия не работала в том случае, если первый блок нам подходит, но block->next == NULL и возвращается неверный флаг

  // нечего перебирать - даже этот невалидный
  if ( !block ) return (struct block_search_result){ .type=BSR_REACHED_END_NOT_FOUND, .block = block };
  if ( block->is_free && block_is_big_enough( sz, block ))
      return (struct block_search_result){ .type=BSR_FOUND_GOOD_BLOCK, .block = block };
  // смотрим все блоки, 
  while( block->next ){
    // собираем вместе те, которые можем - чтобы не было мелких блоков
    while( try_merge_with_next(block) ){}
    // свободный и достаточно большой - нашли
    if ( block->is_free && block_is_big_enough( sz, block ))
      return (struct block_search_result){ .type=BSR_FOUND_GOOD_BLOCK, .block = block };
    // we can't iterate by blocks
    if( block->next == block ) return (struct block_search_result){ .type=BSR_CORRUPTED, .block = block };
    // итератор
    block = block->next;
  }
  return (struct block_search_result) { .type=BSR_REACHED_END_NOT_FOUND, .block = block };
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
  // ищем блок в уже существующей памяти
  struct block_search_result result = find_good_or_last( block, query );
  // нашли - занимаем
  if ( result.type == BSR_FOUND_GOOD_BLOCK && split_if_too_big( result.block, query ) ){
    result.block->is_free = false;
  }
  // прокидываем на дальшейшую обработку регион - в существующей памяти не нашлось столько места.
  return result;
}

static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
  if ( !last ) return NULL;
  // создаем новую страницу с помощью mmap
  struct region r = alloc_region( last->contents + last->capacity.bytes, query );
  // если память выделилась прямо за последним блоком (последний блок свободен!)- расширяем его и возвращаем обратно 
  if ( last->is_free && !region_is_invalid( &r ) && r.extends ){
    last->capacity.bytes += r.size;
    return last;
  }
  // иначе - указатель последнего существующего блока на новую страницу
  last->next = r.addr;
  // вызвращаем указатель на новый блок
  return r.addr;
}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
  // не создаем слишком маленьких блоков
  query = size_max(query, BLOCK_MIN_CAPACITY);
  // пытаемся выделить память в существующей куче
  struct block_search_result result = try_memalloc_existing( query, heap_start );
  // если что-то пошло не так
  if ( result.type == BSR_CORRUPTED ) return NULL;
  if ( result.type == BSR_REACHED_END_NOT_FOUND ){ 
    // если не удалось привлекаем mmap
    struct block_header* block = grow_heap( result.block, query );
    // потом еще раз выделяем из существующей - только либо последний стал больше, либо последний - страница mmap
    result = try_memalloc_existing( query, block );
  // если что-то пошло не так
    if ( result.type == BSR_CORRUPTED ) return NULL;
  } 
  // возврат блока
  return result.block;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  // сливаем освобожденный блок со следующими.
  while(try_merge_with_next( header )){};
}
