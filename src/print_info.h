#ifndef _print_info_
#define _print_info_
#include <stdio.h>

void print_info( const char* const message, FILE* stream );
void println_info( const char* const message, FILE* stream );
void print_pointer( void * message, FILE* stream );
void print_size_t( size_t message, FILE* stream );

#endif
